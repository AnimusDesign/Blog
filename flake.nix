{
  description = "Development environment for website.";
  inputs.nixpkgs.url = "nixpkgs/nixos-22.11";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        devShells.default =
          pkgs.mkShell {
            packages = with pkgs;
              [
                zola
                rustc
                rustfmt
                cargo
              ];
          };
      });
}
